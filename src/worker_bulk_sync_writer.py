import time
import threading
import asyncio
import traceback
from threading import Thread, Event, Condition
from queue import SimpleQueue
from redis.asyncio import Redis

from .stateful_object import StatefulObject
from .eventful_object import EventfulObject
from .async_simple_queue import AsyncSimpleQueue

class WorkerBulkSyncWriter(StatefulObject, EventfulObject, Thread):

    def __init__(self, output_redis_client: Redis, input_queue: AsyncSimpleQueue = None, loop = None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.loop = loop
        self.output_redis_client: Redis = output_redis_client

        self.event_finished: Event = Event()
        self.input_queue: AsyncSimpleQueue = input_queue if input_queue is not None else AsyncSimpleQueue()
        
        # self.output_last_update_condition: Condition = output_last_update_condition if output_last_update_condition is not None else Condition()
        # self.output_last_sent_update: float = 0
        # self.output_last_received_command: float = 0
        # self.output_buffered_items: int = 0

        self.source_delta = 0
        self.target_delta = 0

        now = time.time_ns() / 1_000_000

        self.bulk_data_output_chunk = []
        self.last_received_chunk = now
        self.last_flush = now
        self.first_received_chunk = None

        self.completed_receive = False
        self.successful = False
        self.run_future = None

    async def stop(self):
        if self.input_queue is not None:
            self.input_queue.put_nowait(None)

    async def close(self):
        if self.input_queue is not None:
            self.input_queue.put_nowait(None)
        await self.set_state('stopping')

    async def handle_state_new(self, *args, **kwargs):
        await self.set_state('starting')

    async def handle_state_starting(self, *args, **kwargs):
        now = time.time_ns() / 1_000_000
        self.bulk_data_output_chunk = []
        self.last_received_chunk = now
        self.last_flush = now
        self.first_received_chunk = None
        self.publish('starting', self)
        await self.set_state('receive')

    async def handle_state_receive(self, *args, **kwargs):

        if self.input_queue is None:
            return await self.set_state('flush')

        await self.set_state('receiving')

        try:
            bulk_data_input_chunk = await self.input_queue.get(timeout=0.5)
            
            now = time.time_ns() / 1_000_000

            if bulk_data_input_chunk is None:
                self.input_queue = None
                self.completed_receive = True
                return await self.set_state('flush')

            self.bulk_data_output_chunk.extend(bulk_data_input_chunk)
            self.last_received_chunk = now
            if self.first_received_chunk is None:
                self.first_received_chunk = now

            if len(self.bulk_data_output_chunk) >= 1000 or (len(self.bulk_data_output_chunk) > 0 and (now - self.last_flush) >= 10000):
                return await self.set_state('flush')

        except:
            now = time.time_ns() / 1_000_000
            if len(self.bulk_data_output_chunk) > 0 and (now - self.last_flush) >= 10000:
                return await self.set_state('flush')

        return await self.set_state('receive')

    async def before_leave_state_receive(self, new_state: str, *args, **kwargs):
        if new_state != 'stopping' and self.input_queue is None:
            return 'flush'
        return None

    async def before_leave_state_receiving(self, new_state: str, *args, **kwargs):
        if new_state != 'stopping' and self.input_queue is None:
            return 'flush'
        return None

    async def before_leave_state_flush(self, new_state: str, *args, **kwargs):
        if self.input_queue is None and len(self.bulk_data_output_chunk) == 0:
            self.successful = self.completed_receive
            return 'stopping'
        return None

    async def handle_state_flush(self, previous_state: str, *args, **kwargs):

        if previous_state == 'flush' or previous_state == 'flushing':
            next_state = 'receive'
        else:
            next_state = previous_state

        current_chunk = self.bulk_data_output_chunk

        if previous_state == 'stopping':
            self.bulk_data_output_chunk = []

        if len(current_chunk) == 0:
            return await self.set_state(next_state)

        await self.set_state('flushing')

        try:
            await self.__send_batch(current_chunk)
            self.bulk_data_output_chunk = []

        except Exception as e:
            #print(traceback.format_exception(e))
            # if next_state != 'stopping':
            #     next_state = 'flush'
            #     await asyncio.sleep(1)
            await self.set_state('stopping')
            self.publish('disconnect', self)

        finally:
            await self.set_state(next_state)

    async def handle_state_stopping(self, previous_state: str, *args, **kwargs):
        self.input_queue = None

        if len(self.bulk_data_output_chunk) != 0:
            return await self.set_state('flush')

        self.successful = self.completed_receive

        await self.set_state('finished')

    async def before_leave_state_stopping(self, new_state: str, *args, **kwargs):
        if new_state != 'finished' or (new_state == 'flush' and len(self.bulk_data_output_chunk) == 0):
            return 'finished'
        return None

    async def can_leave_state_stopping(self, new_state: str, *args, **kwargs):
        return new_state == 'finished' or new_state == 'flush'

    async def can_leave_state_finished(self, *args, **kwargs):
        return False

    async def handle_state_finished(self, *args, **kwargs):
        self.event_finished.set()
        if self.successful:
            self.publish('completed', self)
        self.publish('finished', self)
        self.run_future.cancel()

    def run(self):
        if self.loop is None:
            self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.run_future = asyncio.run_coroutine_threadsafe(self._run(), self.loop)
        try:
            return self.run_future.result()
        except:
            return None
        finally:
            self.publish('finished', self)

    async def _run(self):
        await self.set_state('new')
        state, state_version = self.get_state_version()
        previous_state = state
        try:
            while not self.event_finished.is_set():
                try:
                    if state is not None:
                        _, state_version = self.get_state_version()
                        await self.handle_state(previous_state=previous_state)
                        previous_state = state
                    state = await self.wait_state(version=state_version, timeout=1)
                except asyncio.CancelledError:
                    await self.set_state('stopping')
                    pass
        except:
            pass
        finally:
            previous_state, _ = self.get_state_version()
            if previous_state != 'finished' and await self.set_state('finished'):
                await self.handle_state(previous_state=previous_state)

    async def __send_batch(self, bulk_data_chunk):
        if len(bulk_data_chunk) == 0:
            return

        local_timestamp_ms = round(time.time_ns() / 1_000_000)

        keydata = dict()
        for source_time, key, pttl, dump in bulk_data_chunk:
            if pttl == -2:
                continue
            if pttl == -1:
                keydata[key] = (None, dump)
                continue

            source_timestamp_s, source_timestamp_ns_fraction = source_time
            source_timestamp_ms = round(int(source_timestamp_s) * 1000 + int(source_timestamp_ns_fraction) / 1000)

            source_timestamp_ms_adjusted = source_timestamp_ms - self.source_delta

            pttl = source_timestamp_ms_adjusted + pttl
            if pttl <= local_timestamp_ms:
                continue
            keydata[key] = (pttl, dump)

        local_timestamp_ms = round(time.time_ns() / 1_000_000)

        async with self.output_redis_client.pipeline(transaction=False) as pipe:
            for key in keydata:
                pttl, dump = keydata[key]
                if pttl is None:
                    pttl = 0
                elif pttl <= local_timestamp_ms:
                    continue
                else:
                    pttl = round(pttl + self.target_delta)
                await pipe.restore(name=key, ttl=pttl, value=dump, replace=True, absttl=True if pttl != 0 else False)
            await pipe.execute()

        print('+')

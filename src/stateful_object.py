import time
import threading
from collections import deque
from threading import Condition
from asyncio import events, wait_for, exceptions

from .rwlock import RWLock

_global_lock = threading.Lock()

class StatefulObject:
    _loop = None

    def __init__(self, state: str = 'new', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.state = state
        self.state_version: int = 0
        self.state_lock: RWLock = RWLock()
        self.debug = False

        # Futures.
        self._waiters = deque()

    def _get_loop(self):
        loop = events.get_running_loop()

        if self._loop is None:
            with _global_lock:
                if self._loop is None:
                    self._loop = loop

        if loop is not self._loop:
            raise RuntimeError(f'{self!r} is bound to a different event loop')

        return loop

    def _wakeup_next(self, waiters):
        # Wake up the next waiter (if any) that isn't cancelled.
        while waiters:
            waiter = waiters.popleft()
            if not waiter.done():
                waiter.set_result(None)
                break

    async def set_state(self, state: str):
        is_changed: bool = False
        previous_state: str = None

        with self.state_lock.r_locked():
            self_state = self.state
            self_state_version = self.state_version

            if state == self_state:
                return False
            
            previous_state = self_state
            previous_version = self_state_version
            
            handle_before_leave_state_method = 'before_leave_state_' + previous_state
            if hasattr(self, handle_before_leave_state_method) and callable(getattr(self, handle_before_leave_state_method)):
                visited_before_leave_handlers = set()
                current_new_state = state
                new_state = state
                while new_state is not None:
                    if current_new_state in visited_before_leave_handlers:
                        raise Exception('Logic error calling before_leave_state handler: circular loop detected in returned states')
                    visited_before_leave_handlers.add(current_new_state)
                    new_state = await getattr(self, handle_before_leave_state_method)(previous_state=previous_state, new_state=current_new_state)
                    if new_state == current_new_state:
                        new_state = None
                    elif new_state is not None:
                        current_new_state = new_state
                state = current_new_state

                if state == previous_state:
                    return False

            can_leave_state_method = 'can_leave_state_' + previous_state
            if hasattr(self, can_leave_state_method) and callable(getattr(self, can_leave_state_method)):
                if not await getattr(self, can_leave_state_method)(previous_state=previous_state, new_state=state):
                    return False

            can_set_state_method = 'can_set_state_' + state
            if hasattr(self, can_set_state_method) and callable(getattr(self, can_set_state_method)):
                if not await getattr(self, can_set_state_method)(previous_state=previous_state, new_state=state):
                    return False
        
        with self.state_lock.w_locked():
            if previous_version != self.state_version or previous_state != self.state:
                raise Exception('Logic error during set_state: before_leave_state, can_leave_state and can_set_state callback methods must not modify current state')

            self.state = state
            self.state_version = self.state_version + 1
            current_state = self.state
            current_version = self.state_version
            
        if self.debug:
            print(current_state, current_version)

        with self.state_lock.r_locked():
            #try:
            handle_leave_state_method = 'handle_leave_state_' + previous_state
            if hasattr(self, handle_leave_state_method) and callable(getattr(self, handle_leave_state_method)):
                await getattr(self, handle_leave_state_method)(previous_state=previous_state, new_state=state)

            if current_state != self.state or current_version != self.state_version:
                raise Exception('Logic error during set_state: handle_leave_state callback method must not modify current state')

            handle_set_state_method = 'handle_set_state_' + state
            if hasattr(self, handle_set_state_method) and callable(getattr(self, handle_set_state_method)):
                await getattr(self, handle_set_state_method)(previous_state=previous_state, new_state=state)

            if current_state != self.state or current_version != self.state_version:
                raise Exception('Logic error during set_state: handle_set_state callback method must not modify current state')
            #finally:
            #    self.state_condition.notify_all()

        return True

    def get_state(self):
        state, version = self.get_state_version()
        return state

    def get_state_version(self):
        with self.state_lock.r_locked():
            return self.state, self.state_version

    async def wait_state(self, version: int = None, timeout: float = None):
        if timeout is not None:
            try:
                return await wait_for(self.wait_state(version=version,timeout=None), timeout=timeout)
            except exceptions.TimeoutError:
                return None

        with self.state_lock.r_locked():
            current_state_version = self.state_version if version is None else version
        while True:
            with self.state_lock.r_locked():
                self_state_version = self.state_version
                self_state = self.state
            if self_state_version != current_state_version:
                return self_state
            waiter = self._get_loop().create_future()
            self._waiters.append(waiter)
            try:
                await waiter
            except:
                waiter.cancel()  # Just in case waiter is not done yet.
                try:
                    # Clean self._waiters from canceled waiter.
                    self._waiters.remove(waiter)
                except ValueError:
                    pass
                if not waiter.cancelled():
                    self._wakeup_next(self._waiters)
                raise

    async def handle_state(self, previous_state: str):
        state = self.get_state()
        handler_method = 'handle_state_' + state
        if hasattr(self, handler_method) and callable(getattr(self, handler_method)):
            return await getattr(self, handler_method)(state=state, previous_state=previous_state)
        return None
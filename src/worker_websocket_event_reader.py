import time
import threading
import redis
import asyncio
import traceback
import pickle
from threading import Thread, Event, Condition, Lock
from queue import SimpleQueue
from redis.asyncio import Redis
from collections import deque

from asyncer import syncify

from fastapi import (
    WebSocket,
)

from websockets.exceptions import ConnectionClosedOK

from .command_parser import CommandParser
from .header_parser import HeaderParser
from .redis_monitor import RedisMonitor

from .stateful_object import StatefulObject
from .eventful_object import EventfulObject
from .async_simple_queue import AsyncSimpleQueue


class WorkerWebsocketEventReader(StatefulObject, EventfulObject, Thread):

    def __init__(self, input_websocket: WebSocket, loop = None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.loop = loop
        self.input_websocket: WebSocket = input_websocket

        self.event_stopping: Event = Event()
        self.event_finished: Event = Event()

        self.run_future = None

    async def handle_state_new(self, *args, **kwargs):
        await self.set_state('starting')

    async def handle_state_starting(self, *args, **kwargs):
        self.publish('starting', self)
        await self.set_state('receive')

    async def before_set_state_receive(self, *args, **kwargs):
        if self.event_stopping.is_set():
            return 'stopping'
        return None

    async def before_set_state_receiving(self, *args, **kwargs):
        if self.event_stopping.is_set():
            return 'stopping'
        return None


    async def handle_state_receive(self, *args, **kwargs):
        try:
            await self.set_state('receiving')
            data = await self.input_websocket.recv()
            event, incoming = pickle.loads(data)
            self.publish(event, incoming)

        except ConnectionClosedOK:
            return await self.set_state('disconnect')

        except Exception as e:
            return await self.set_state('disconnect')

        except asyncio.CancelledError:
            return await self.set_state('stopping')
        
        await self.set_state('receive')

    async def before_set_state_disconnect(self, *args, **kwargs):
        if self.event_stopping.is_set():
            return 'stopping'
        return None

    async def handle_state_disconnect(self, *args, **kwargs):
        #await self.set_state('receive')
        await self.set_state('stopping')
        if not self.event_stopping.is_set():
            await asyncio.sleep(1)

    async def before_set_state_stopping(self, *args, **kwargs):
        self.event_stopping.set()
        return None

    async def can_leave_state_stopping(self, new_state: str, *args, **kwargs):
        return new_state == 'finished'

    async def can_leave_state_finished(self, *args, **kwargs):
        return False

    async def handle_state_stopping(self, *args, **kwargs):
        await self.set_state('finished')

    async def handle_state_finished(self, *args, **kwargs):
        self.event_finished.set()
        self.publish('finished', self)
        self.run_future.cancel()

    async def close(self):
        await self.set_state('stopping')

    def run(self):
        if self.loop is None:
            self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.run_future = asyncio.run_coroutine_threadsafe(self._run(), self.loop)
        try:
            return self.run_future.result()
        except Exception as e:
            #print(*traceback.format_exception(e))
            return None
        finally:
            self.publish('finished', self)

    async def _run(self):
        await self.set_state('new')
        state, state_version = self.get_state_version()
        previous_state = state
        try:
            while not self.event_finished.is_set():
                try:
                    if state is not None:
                        _, state_version = self.get_state_version()
                        await self.handle_state(previous_state=previous_state)
                        previous_state = state
                    state = await self.wait_state(version=state_version, timeout=0.1)
                except asyncio.CancelledError:
                    await self.set_state('stopping')
                    pass
        except Exception as e:
            #print(*traceback.format_exception(e))
            pass
            
        finally:
            previous_state = self.get_state()
            if previous_state != 'finished' and await self.set_state('finished'):
                await self.handle_state(previous_state=previous_state)
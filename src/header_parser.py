from typing import Optional, Mapping

class HeaderParser:

    #
    # Line format example:
    #
    # 1707143890.930256 [0 172.17.0.1:58748] "SETEX" "PHPREDIS_SESSION:*******" "14400" "loginView|s:5:\"login\";"
    #

    # Return: ( command_offset, (timestamp, db, ip_port) )
    # Return raw values from byte strings so if we can choose to not parse values we dont need
    def parse_header(self, line: bytes, offset: int = 0) -> tuple[Optional[int], Optional[tuple[bytes, bytes, bytes]]]:
        result = []
        state = 0

        result_offset = None
        line_length = len(line)

        current_value = []

        for current_offset in range(offset, line_length):
            current_byte = line[current_offset]

            if state == 0:
                if current_byte == 0x20:
                    state = 1
                    result.append(bytes(current_value))
                    current_value = [] 
                    continue
                current_value.append(current_byte)
                continue

            elif state == 1:
                if current_byte != 0x5B:
                    return (None, None)
                state = 2
                continue

            elif state == 2:
                if current_byte == 0x20:
                    state = 3
                    result.append(bytes(current_value))
                    current_value = []
                    continue
                current_value.append(current_byte)
                continue

            elif state == 3:
                current_value.append(current_byte)
                if current_byte == 0x5B:
                    state = 4
                else:
                    state = 5
                continue

            elif state == 4:
                current_value.append(current_byte)
                if current_byte == 0x5D:
                    state = 5
                continue

            elif state == 5:
                if current_byte == 0x5D:
                    state = 6
                    result.append(bytes(current_value))
                    current_value = []
                    continue
                current_value.append(current_byte)
                continue

            elif state == 6:
                if current_byte != 0x20:
                    return (None, None)
                state = 7
                continue

            elif state == 7:
                result_offset = current_offset
                break

            else:
                return (None, None)


        if result_offset is None:
            return (None, None)

        return (result_offset, (result[0], result[1], result[2]))
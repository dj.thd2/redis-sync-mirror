import time
import threading
import redis
import asyncio
from threading import Thread, Event, Condition, Lock
from queue import SimpleQueue
from redis.asyncio import Redis
from collections import deque

from asyncer import syncify

from .command_parser import CommandParser
from .header_parser import HeaderParser
from .redis_monitor import RedisMonitor

from .stateful_object import StatefulObject
from .eventful_object import EventfulObject
from .async_simple_queue import AsyncSimpleQueue


class WorkerMirroringReader(StatefulObject, EventfulObject, Thread):

    BUFFER_MAX_LENGTH = 5000

    def __init__(self, input_redis_client: Redis, command_parser: CommandParser, header_parser: HeaderParser, loop = None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.loop = loop
        self.input_redis_client: Redis = input_redis_client
        self.command_parser: CommandParser = command_parser
        self.header_parser: HeaderParser = header_parser

        self.event_stopping: Event = Event()
        self.event_finished: Event = Event()

        self.buffer = deque(maxlen=self.BUFFER_MAX_LENGTH)
        self.buffer_lock = Lock()

        self.run_future = None

    def subscribe_from_index(self, queue: AsyncSimpleQueue, begin_index: int = None):
        if begin_index is None:
            return self.subscribe(['mirrordata'], queue)
        with self.buffer_lock:
            if len(self.buffer) > 0 and self.buffer[0][0] <= begin_index and self.buffer[-1][0] >= begin_index:
                for item in self.buffer:
                    if item[0] > begin_index:
                        queue.put_nowait(('mirrordata', item))
        return self.subscribe(['mirrordata'], queue)


    async def handle_state_new(self, *args, **kwargs):
        await self.set_state('starting')

    async def handle_state_starting(self, *args, **kwargs):
        self.publish('starting', self)
        await self.set_state('receive')

    async def before_set_state_receive(self, *args, **kwargs):
        if self.event_stopping.is_set():
            return 'stopping'
        return None

    async def before_set_state_receiving(self, *args, **kwargs):
        if self.event_stopping.is_set():
            return 'stopping'
        return None

    async def handle_state_receive(self, *args, **kwargs):
        try:
            await self.set_state('receiving')
            async with RedisMonitor(self.input_redis_client.connection_pool) as monitor:
                commands = monitor.listen()
                
                async for c in commands:
                    if self.event_stopping.is_set():
                        raise

                    command_offset, header = self.header_parser.parse_header(c, 0)

                    if command_offset is None:
                        continue

                    command = self.command_parser.parse_command(c, command_offset)

                    if command is None or len(command) == 0:
                        continue

                    process = True

                    server_timestamp, database, host = header

                    timestamp_s, timestamp_ns = server_timestamp.split(b'.')

                    timestamp_ms = round(int(timestamp_s) * 1000 + int(timestamp_ns) / 1000)
                    adjusted_timestamp_ms = timestamp_ms + monitor.delta_ms
                    local_timestamp_ms = round(time.time_ns() / 1_000_000)
                    lag_ms = local_timestamp_ms - adjusted_timestamp_ms

                    # if command[0].upper() == b'SETEX' and len(command) >= 3:
                    #     if command[3] is None or len(command[3]) == 0:
                    #         process = False
                    # if command[0].upper() == b'EXPIRE':
                    #     process = False

                    if process:
                        data = (timestamp_ms, adjusted_timestamp_ms, header, command)
                        with self.buffer_lock:
                            self.buffer.append(data)
                        self.publish('mirrordata', data)

                    if lag_ms > 15000:
                        return await self.set_state('disconnect')
                
        except Exception as e:
            return await self.set_state('disconnect')

        await self.set_state('receive')

    async def before_set_state_disconnect(self, *args, **kwargs):
        if self.event_stopping.is_set():
            return 'stopping'
        return None

    async def handle_state_disconnect(self, *args, **kwargs):
        self.publish('disconnect', None)
        await self.set_state('receive')
        if not self.event_stopping.is_set():
            await asyncio.sleep(1)

    async def before_set_state_stopping(self, *args, **kwargs):
        self.event_stopping.set()
        return None

    async def can_leave_state_stopping(self, new_state: str, *args, **kwargs):
        return new_state == 'finished'

    async def can_leave_state_finished(self, *args, **kwargs):
        return False

    async def handle_state_stopping(self, *args, **kwargs):
        self.publish('mirrordata', None)
        await self.set_state('finished')

    async def handle_state_finished(self, *args, **kwargs):
        self.event_finished.set()
        self.publish('finished', self)
        self.run_future.cancel()

    async def close(self):
        await self.set_state('stopping')

    def run(self):
        if self.loop is None:
            self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.run_future = asyncio.run_coroutine_threadsafe(self._run(), self.loop)
        try:
            return self.run_future.result()
        except:
            return None
        finally:
            self.publish('finished', self)

    async def _run(self):
        await self.set_state('new')
        state, state_version = self.get_state_version()
        previous_state = state
        try:
            while not self.event_finished.is_set():
                try:
                    if state is not None:
                        _, state_version = self.get_state_version()
                        await self.handle_state(previous_state=previous_state)
                        previous_state = state
                    state = await self.wait_state(version=state_version, timeout=0.1)
                except asyncio.CancelledError:
                    await self.set_state('stopping')
                    pass
        except:
            pass
            
        finally:
            previous_state = self.get_state()
            if previous_state != 'finished' and await self.set_state('finished'):
                await self.handle_state(previous_state=previous_state)
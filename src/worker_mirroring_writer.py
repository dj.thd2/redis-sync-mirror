import time
import asyncio
from threading import Thread, Event, Condition
from queue import SimpleQueue
from redis.asyncio import Redis

from asyncer import syncify

from .stateful_object import StatefulObject
from .eventful_object import EventfulObject
from .async_simple_queue import AsyncSimpleQueue

class WorkerMirroringWriter(StatefulObject, EventfulObject, Thread):

    def __init__(self, output_redis_client: Redis, input_queue: AsyncSimpleQueue = None, loop = None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.loop = loop
        self.output_redis_client: Redis = output_redis_client

        self.event_finished: Event = Event()
        self.input_queue: AsyncSimpleQueue = input_queue if input_queue is not None else AsyncSimpleQueue()

        # self.output_last_update_condition: Condition = output_last_update_condition if output_last_update_condition is not None else Condition()
        # self.output_last_sent_update: float = 0
        # self.output_last_received_command: float = 0

        self.current_message = None
        self.target_raw_connection = None

        self.run_future = None

    async def stop(self):
        if self.input_queue is not None:
            self.input_queue.put_nowait(None)

    async def close(self):
        if self.input_queue is not None:
            self.input_queue.put_nowait(None)
        await self.set_state('stopping')

    async def handle_state_new(self, *args, **kwargs):
        await self.set_state('starting')

    async def handle_state_starting(self, *args, **kwargs):
        self.publish('starting', self)
        await self.set_state('receive')

    async def before_leave_state_receive(self, *args, **kwargs):
        if self.input_queue is None:
            return 'stopping'

    async def handle_state_receive(self, *args, **kwargs):
        self.current_message = None

        if self.input_queue is None:
            return await self.set_state('stopping')

        await self.set_state('receiving')

        try:
            self.current_message = await self.input_queue.get(timeout=0.5)
            if self.current_message is None:
                self.input_queue = None
                return await self.set_state('stopping')
            server_timestamp_ms, adjusted_timestamp_ms, header, command = self.current_message

        except:
            return await self.set_state('receive')

        await self.set_state('process')

    async def before_leave_state_process(self, *args, **kwargs):
        if self.input_queue is None:
            return 'stopping'

    async def handle_state_process(self, *args, **kwargs):
        if self.target_raw_connection is None:
            await self.set_state('connecting')
            try:
                self.target_raw_connection = await self.output_redis_client.connection_pool.get_connection('monitor', None)
                self.publish('connect', self)
            except:
                return await self.set_state('disconnect')
        await self.set_state('processing')
        try:
            server_timestamp_ms, adjusted_timestamp_ms, header, command = self.current_message
            await self.target_raw_connection.send_command(*command, check_health=False)
            await self.target_raw_connection.send_command(b"MSET", b"__redis-sync-last-received", str(server_timestamp_ms).encode(), b"__redis-sync-last-seen", str(adjusted_timestamp_ms).encode(), check_health=False)

        except:
            return await self.set_state('disconnect')

        await self.set_state('receive')

    async def handle_state_disconnect(self, *args, **kwargs):
        if self.target_raw_connection is not None:
            try:
                await self.target_raw_connection.disconnect()
                await self.output_redis_client.connection_pool.release(self.target_raw_connection)
            except:
                pass
            self.publish('disconnect', self)
            self.target_raw_connection = None
        
        if self.input_queue is not None:
            await asyncio.sleep(1)
        await self.set_state('process')

    async def handle_state_stopping(self, *args, **kwargs):
        self.input_queue = None
        if self.target_raw_connection is not None:
            return await self.set_state('disconnect')
        await self.set_state('finished')

    async def before_leave_state_stopping(self, new_state: str, *args, **kwargs):
        if new_state != 'finished' or (new_state == 'disconnect' and self.target_raw_connection is None):
            return 'finished'
        return None

    async def can_leave_state_stopping(self, new_state: str, *args, **kwargs):
        return new_state == 'finished' or new_state == 'disconnect'

    async def can_leave_state_finished(self, *args, **kwargs):
        return False

    async def handle_state_finished(self, *args, **kwargs):
        self.event_finished.set()
        self.publish('finished', self)
        self.run_future.cancel()

    def run(self):
        if self.loop is None:
            self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.run_future = asyncio.run_coroutine_threadsafe(self._run(), self.loop)
        try:
            return self.run_future.result()
        except:
            return None
        finally:
            self.publish('finished', self)

    async def _run(self):
        await self.set_state('new')
        state, state_version = self.get_state_version()
        previous_state = state
        try:
            while not self.event_finished.is_set():
                try:
                    if state is not None:
                        _, state_version = self.get_state_version()
                        await self.handle_state(previous_state=previous_state)
                        previous_state = state
                    state = await self.wait_state(version=state_version, timeout=0.5)
                except asyncio.CancelledError:
                    await self.set_state('stopping')
                    pass
        except:
            pass

        finally:
            previous_state, _ = self.get_state_version()
            if previous_state != 'finished' and await self.set_state('finished'):
                await self.handle_state(previous_state=previous_state)
            if self.target_raw_connection is not None:
                try:
                    await self.target_raw_connection.disconnect()
                    await self.output_redis_client.connection_pool.release(self.target_raw_connection)
                except:
                    pass
            self.target_raw_connection = None

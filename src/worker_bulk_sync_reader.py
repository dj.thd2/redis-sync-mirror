import time
import threading
import asyncio
import traceback
from threading import Thread, Event, Condition
from queue import SimpleQueue
from redis.asyncio import Redis

from asyncer import syncify

from .stateful_object import StatefulObject
from .eventful_object import EventfulObject
from .async_simple_queue import AsyncSimpleQueue

class WorkerBulkSyncReader(StatefulObject, EventfulObject, Thread):

    # TODO: extract to file
    BULK_DUMP_LUA_SCRIPT_7 = '''#!lua flags=no-writes,allow-stale
local result = {}
local time = redis.call('time')
table.insert(result, time)
local current_time = tonumber(time[1])

local min_time = tonumber(ARGV[1])

for _, key in pairs(KEYS) do
  local skip = false
  if min_time ~= nil then
    local idletime = tonumber(redis.call('object', 'idletime', key))
    if idletime ~= nil and (current_time - idletime) <= min_time then
      skip = true
    end
  end
  if not skip then
    table.insert(result, key)
    table.insert(result, redis.call('pttl', key))
    table.insert(result, redis.call('dump', key))
  end
end

table.insert(result, nil)
table.insert(result, nil)
table.insert(result, nil)

return result
    '''

    BULK_DUMP_LUA_SCRIPT_6 = '''
local result = {}
local time = redis.call('time')
table.insert(result, time)
local current_time = tonumber(time[1])

local min_time = tonumber(ARGV[1])

for _, key in pairs(KEYS) do
  local skip = false
  if min_time ~= nil then
    local idletime = tonumber(redis.call('object', 'idletime', key))
    if idletime ~= nil and (current_time - idletime) <= min_time then
      skip = true
    end
  end
  if not skip then
    table.insert(result, key)
    table.insert(result, redis.call('pttl', key))
    table.insert(result, redis.call('dump', key))
  end
end

table.insert(result, nil)
table.insert(result, nil)
table.insert(result, nil)

return result
    '''

    SCAN_ITER_BATCH = 1000
    MAX_RECEIVE_BATCH = 2500
    KEY_NAMES_CHUNK_SIZE = 2000

    def __init__(self, input_redis_client: Redis, loop = None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.loop = loop
        self.input_redis_client: Redis = input_redis_client

        self.event_stopping: Event = Event()
        self.event_finished: Event = Event()

        self.run_future = None

    async def handle_state_new(self, *args, **kwargs):
        await self.set_state('starting')

    async def handle_state_starting(self, *args, **kwargs):
        self.publish('starting', self)
        await self.set_state('receive')

    async def handle_state_receive(self, *args, **kwargs):
        key_names = []
        finished_successfully = False

        try:
            async for key in self.input_redis_client.scan_iter(count=self.SCAN_ITER_BATCH):
                key_names.append(key)

                if self.event_stopping.is_set():
                    raise

                if len(key_names) >= self.MAX_RECEIVE_BATCH:
                    key_dumps = await self.__dump_key_names(key_names)
                    self.publish('syncdata', key_dumps)
                    key_names = []

            finished_successfully = True

        except asyncio.CancelledError:
            return await self.set_state('stopping')

        except:
            pass

        
        if finished_successfully and not self.event_stopping.is_set():
            try:
                if len(key_names) > 0:
                    key_dumps = await self.__dump_key_names(key_names)
                    self.publish('syncdata', key_dumps)
                    key_names = []
                self.publish('syncdata', None)
                self.publish('completed', self)

            except asyncio.CancelledError:
                return await self.set_state('stopping')

            except:
                pass
        
        await self.set_state('stopping')

    async def handle_set_state_stopping(self, *args, **kwargs):
        self.event_stopping.set()

    async def can_leave_state_stopping(self, new_state: str, *args, **kwargs):
        return new_state == 'finished'

    async def can_leave_state_finished(self, *args, **kwargs):
        return False

    async def handle_state_stopping(self, *args, **kwargs):
        self.event_stopping.set()
        await self.set_state('finished')

    async def handle_state_finished(self, *args, **kwargs):
        self.event_finished.set()
        self.publish('finished', self)
        self.run_future.cancel()
        
    async def close(self):
        await self.set_state('stopping')

    def run(self):
        if self.loop is None:
            self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.run_future = asyncio.run_coroutine_threadsafe(self._run(), self.loop)
        try:
            return self.run_future.result()
        except:
            return None
        finally:
            self.publish('finished', self)

    async def _run(self):
        await self.set_state('new')
        state, state_version = self.get_state_version()
        previous_state = state
        try:
            while not self.event_finished.is_set():
                try:
                    if state is not None:
                        _, state_version = self.get_state_version()
                        await self.handle_state(previous_state=previous_state)
                        previous_state = state
                    state = await self.wait_state(version=state_version, timeout=0.5)
                except asyncio.CancelledError:
                    await self.set_state('stopping')
                    pass
        except Exception as e:
            #print(traceback.format_exception(e))
            pass
        finally:
            previous_state = self.get_state()
            if previous_state != 'finished' and await self.set_state('finished'):
                await self.handle_state(previous_state=previous_state)



    def __chunks(self, l, n):
        for i in range(0, len(l), n):  
            yield l[i:i + n] 

    async def __dump_key_names(self, key_names):
        script_args = []
        keys_dumps = []

        if not key_names or len(key_names) == 0:
            return keys_dumps

        dump_lua_script = self.input_redis_client.register_script(self.BULK_DUMP_LUA_SCRIPT_6)

        loaded_key_chunks = []

        async with self.input_redis_client.pipeline(transaction=False) as pipe:
            for key_names_chunk in self.__chunks(key_names, self.KEY_NAMES_CHUNK_SIZE):
                await dump_lua_script(keys=key_names_chunk, args=script_args, client=pipe)
            loaded_key_chunks = await pipe.execute()

        for loaded_keys in loaded_key_chunks:
            source_time = loaded_keys[0]

            i = 1
            while i <= len(loaded_keys)-3:
                if loaded_keys[i] is not None:
                    keys_dumps.append((source_time, loaded_keys[i], loaded_keys[i+1], loaded_keys[i+2]))
                i = i + 3

        print('.')
        return keys_dumps

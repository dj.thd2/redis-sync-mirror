from typing import Optional, Mapping

EnabledCommandsMapping = Optional[Mapping[bytes, Optional[Mapping]]]

class CommandParser:

    # Done following the info from redis docs (https://redis.io/docs/connect/cli):
    #
    # Quoted string values are enclosed in double (") or single (') quotation marks. Escape sequences are used to put nonprintable characters in character and string literals.
    #
    # An escape sequence contains a backslash (\) symbol followed by one of the escape sequence characters.
    #
    # Doubly-quoted strings support the following escape sequences:
    #
    #   \" - double-quote
    #   \n - newline
    #   \r - carriage return
    #   \t - horizontal tab
    #   \b - backspace
    #   \a - alert
    #   \\ - backslash
    #   \xhh - any ASCII character represented by a hexadecimal number (hh)
    #
    # Single quotes assume the string is literal, and allow only the following escape sequences:
    #
    #   \' - single quote
    #   \\ - backslash

    REDIS_ESCAPE_SEQUENCES = {
        0x5C: 0x5C,
        0x22: 0x22,
        0x4E: 0x0A,
        0x52: 0x0D,
        0x54: 0x09,
        0x42: 0x08,
        0x41: 0x07,
    }

    def __init__(self, enabled_command_list: list[bytes] = None):
        self.enabled_commands: EnabledCommandsMapping = self.__process_command_list(enabled_command_list if enabled_command_list is not None else [])

    def __process_command_list(self, command_list: list[bytes] = None) -> EnabledCommandsMapping:
        result: EnabledCommandsMapping = dict()
        if command_list is None:
            return None
        for compound_command in command_list:
            if len(compound_command) == 0:
                continue
            command = compound_command.pop(0)
            if command not in result:
                result[command] = []
            result[command].append(compound_command)
        if len(result) == 0:
            return None
        for command in result:
            result[command] = self.__process_command_list(result[command])
        return result

    def parse_command(self, command: bytes, offset: int = 0) -> Optional[list[bytes]]:
        result = []
        state = 0
        current_command = []
        current_command_upper = []

        command_length = len(command)

        last_command_offset = offset

        enabled_commands = self.enabled_commands

        for current_offset in range(offset, command_length):
            current_byte = command[current_offset]
            current_byte_upper = (current_byte - 0x20) if current_byte > 0x60 else current_byte

            if state == 0:
                if current_byte == 0x20:
                    continue
                if current_byte == 0x22:
                    state = 2
                    continue
                state = 1
                current_command.append(current_byte)
                if enabled_commands is not None:
                    current_command_upper.append(current_byte_upper)
                continue

            elif state == 1:
                if current_byte == 0x20:
                    if enabled_commands is not None:
                        current_command_upper = bytes(current_command_upper)
                        if current_command_upper not in enabled_commands:
                            return None
                        enabled_commands = enabled_commands[current_command_upper]
                        current_command_upper = []
                    result.append(bytes(current_command))
                    current_command = []
                    state = 0
                    continue
                current_command.append(current_byte)
                if enabled_commands is not None:
                    current_command_upper.append(current_byte_upper)
                continue

            elif state == 2:
                if current_byte == 0x22:
                    if enabled_commands is not None:
                        current_command_upper = bytes(current_command_upper)
                        if current_command_upper not in enabled_commands:
                            return None
                        enabled_commands = enabled_commands[current_command_upper]
                        current_command_upper = []
                    result.append(bytes(current_command))
                    current_command = []
                    state = 0
                    continue
                if current_byte == 0x5C:
                    state = 3
                    continue
                current_command.append(current_byte)
                if enabled_commands is not None:
                    current_command_upper.append(current_byte_upper)
                continue

            elif state == 3:
                if current_byte_upper == 0x58:
                    state = 4
                    continue
                state = 2
                if current_byte_upper in CommandParser.REDIS_ESCAPE_SEQUENCES:
                    current_command.append(CommandParser.REDIS_ESCAPE_SEQUENCES[current_byte_upper])
                    if enabled_commands is not None:
                        current_command_upper.append(CommandParser.REDIS_ESCAPE_SEQUENCES[current_byte_upper])
                    continue
                current_command.extend([0x5C, current_byte])
                if enabled_commands is not None:
                    current_command_upper.extend([0x5C, current_byte_upper])
                continue

            elif state == 4:
                if current_byte_upper < 0x30 or current_byte_upper > 0x46 or (current_byte_upper > 0x39 and current_byte_upper < 0x41):
                    current_command.extend([command[current_offset-2], command[current_offset-1], current_byte])
                    if enabled_commands is not None:
                        # HACK: do not bother converting to uppercase the previous bytes, as the command names should never contain escape sequences and they are anyway not going to be in allowlist
                        current_command_upper.extend([command[current_offset-2], command[current_offset-1], current_byte_upper])
                    state = 2
                    continue
                state = 5
                continue

            elif state == 5:
                state = 2
                if current_byte_upper < 0x30 or current_byte_upper > 0x46 or (current_byte_upper > 0x39 and current_byte_upper < 0x41):
                    current_command.extend([command[current_offset-3], command[current_offset-2], command[current_offset-1], current_byte])
                    if enabled_commands is not None:
                        # HACK: do not bother converting to uppercase the previous bytes, as the command names should never contain escape sequences and they are anyway not going to be in allowlist
                        current_command_upper.extend([command[current_offset-3], command[current_offset-2], command[current_offset-1], current_byte_upper])
                    continue
                previous_byte = command[current_offset-1]
                previous_byte_upper = (previous_byte - 0x20) if previous_byte > 0x60 else previous_byte
                current_byte = (previous_byte_upper - 0x37 if previous_byte_upper > 0x40 else previous_byte_upper - 0x30) * 0x10 + (current_byte_upper - 0x37 if current_byte_upper > 0x40 else current_byte_upper - 0x30)
                current_command.append(current_byte)
                if enabled_commands is not None:
                    # HACK: do not bother converting to uppercase the current byte, as the command names should never contain escape sequences and they are anyway not going to be in allowlist
                    current_command_upper.append(current_byte)
                continue

        if state == 0:
            current_command = None
            current_command_upper = None

        elif state == 1 or state == 2:
            current_command = bytes(current_command)
            if enabled_commands is not None:
                current_command_upper = bytes(current_command_upper)
            state = 0

        if state == 3:
            current_command.extend(command[-1:])
            current_command = bytes(current_command)
            if enabled_commands is not None:
                current_command_upper.extend(command[-1:])
                current_command_upper = bytes(current_command_upper)
            state = 0

        elif state == 4:
            current_command.extend(command[-2:])
            current_command = bytes(current_command)
            if enabled_commands is not None:
                current_command_upper.extend(command[-2:])
                current_command_upper = bytes(current_command_upper)
            state = 0

        elif state == 5:
            current_command.extend(command[-3:])
            current_command = bytes(current_command)
            if enabled_commands is not None:
                current_command_upper.extend(command[-3:])
                current_command_upper = bytes(current_command_upper)
            state = 0

        if enabled_commands is not None:
            if current_command_upper is None:
                return None
            if current_command_upper not in enabled_commands:
                return None
            if enabled_commands[current_command_upper] is not None:
                return None

        if current_command is not None:
            result.append(current_command)

        return result
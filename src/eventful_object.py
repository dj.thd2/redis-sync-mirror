from queue import SimpleQueue
from threading import Condition, Lock
from .async_simple_queue import AsyncSimpleQueue

class EventfulObject:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue_lock = Lock()
        self.queues = set()
        self.subscribers = dict()
        self.queues_only_payload = set()
        self.subscribers_only_payload = dict()

    def subscribe(self, events, queue: AsyncSimpleQueue = None, only_payload: bool = False):
        if queue is None:
            queue = AsyncSimpleQueue()
        with self.queue_lock:
            if not only_payload:
                self.queues.add(queue)
                for event in events:
                    if event not in self.subscribers:
                        self.subscribers[event] = set()
                    self.subscribers[event].add(queue)
            else:
                self.queues_only_payload.add(queue)
                for event in events:
                    if event not in self.subscribers_only_payload:
                        self.subscribers_only_payload[event] = set()
                    self.subscribers_only_payload[event].add(queue)
        return queue

    def unsubscribe(self, queue: AsyncSimpleQueue):
        with self.queue_lock:
            self.queues.remove(queue)
            for event in self.subscribers:
                self.subscribers[event].remove(queue)
            self.queues_only_payload.remove(queue)
            for event in self.subscribers_only_payload:
                self.subscribers_only_payload[event].remove(queue)

    def publish(self, event: str, payload):
        with self.queue_lock:
            if event in self.subscribers:
                message = (event, payload)
                for queue in self.subscribers[event]:
                    queue.put_nowait(message)
            if event in self.subscribers_only_payload:
                for queue in self.subscribers_only_payload[event]:
                    queue.put_nowait(payload)

    def publish_multi(self, messages):
        with self.queue_lock:
            for message in messages:
                event, payload = message
                if event in self.subscribers:
                    for queue in self.subscribers[event]:
                        queue.put_nowait(message)
                if event in self.subscribers_only_payload:
                    for queue in self.subscribers_only_payload[event]:
                        queue.put_nowait(payload)

import threading
from collections import deque
from queue import SimpleQueue, Empty
from asyncio import events, locks, wait_for, exceptions

_global_lock = threading.Lock()

# https://github.com/python/cpython/blob/3.12/Lib/asyncio/queues.py
# https://github.com/python/cpython/blob/3.12/Lib/asyncio/mixins.py

class AsyncSimpleQueue:
    _loop = None

    def __init__(self):
        self._queue = SimpleQueue()

        # Futures.
        self._getters = deque()

        # Futures.
        # self._putters = deque()

    def _get_loop(self):
        loop = events.get_running_loop()

        if self._loop is None:
            with _global_lock:
                if self._loop is None:
                    self._loop = loop

        if loop is not self._loop:
            raise RuntimeError(f'{self!r} is bound to a different event loop')

        return loop

    def _wakeup_next(self, waiters):
        # Wake up the next waiter (if any) that isn't cancelled.
        while waiters:
            waiter = waiters.popleft()
            if not waiter.done():
                waiter.set_result(None)
                break

    def qsize(self):
        return self._queue.qsize()

    def empty(self):
        return self._queue.empty()

    async def put(self, item, block=True, timeout=None):
        return self.put_nowait(item)

    def put_nowait(self, item):
        """Put an item into the queue without blocking.

        If no free slot is immediately available, raise QueueFull.
        """
        self._queue.put_nowait(item)
        self._wakeup_next(self._getters)

    async def get(self, block=True, timeout=None):
        """Remove and return an item from the queue.

        If queue is empty, wait until an item is available.
        """

        if not block:
            return self.get_nowait()

        if timeout is not None:
            try:
                return await wait_for(self.get(block=True), timeout=timeout)
            except exceptions.TimeoutError:
                raise Empty()
        
        while self.empty():
            getter = self._get_loop().create_future()
            self._getters.append(getter)
            try:
                await getter
            except:
                getter.cancel()  # Just in case getter is not done yet.
                try:
                    # Clean self._getters from canceled getters.
                    self._getters.remove(getter)
                except ValueError:
                    # The getter could be removed from self._getters by a
                    # previous put_nowait call.
                    pass
                if not self.empty() and not getter.cancelled():
                    # We were woken up by put_nowait(), but can't take
                    # the call.  Wake up the next in line.
                    self._wakeup_next(self._getters)
                raise

        return self.get_nowait()

    def get_nowait(self):
        """Remove and return an item from the queue.

        Return an item if one is immediately available, else raise QueueEmpty.
        """
        item = self._queue.get_nowait()
        # self._wakeup_next(self._putters)
        return item

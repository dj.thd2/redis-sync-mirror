import redis
import time

from redis._parsers.helpers import bool_ok
from redis.exceptions import RedisError
from redis.asyncio.connection import ConnectionPool

class RedisMonitor:
    def __init__(self, connection_pool: ConnectionPool):
        self.connection_pool = connection_pool
        self.connection = None
        self.delta_ms = 0

    async def __aenter__(self):
        self.connection = await self.connection_pool.get_connection("MONITOR")
        await self.connection.send_command("TIME")
        timestamp_s, timestamp_ns = await self.connection.read_response()
        timestamp_ms = round(int(timestamp_s) * 1000 + int(timestamp_ns) / 1000)
        local_timestamp_ms = round(time.time_ns() / 1_000_000)
        self.delta_ms = local_timestamp_ms - timestamp_ms

        await self.connection.send_command("MONITOR")
        # check that monitor returns 'OK', but don't return it to user
        response = await self.connection.read_response()
        if not bool_ok(response):
            raise RedisError(f"MONITOR failed: {response}")
        return self

    async def __aexit__(self, *args):
        await self.connection.disconnect()
        await self.connection_pool.release(self.connection)
        self.connection = None
        self.delta_ms = 0

    async def listen(self):
        while True:
            yield await self.connection.read_response()

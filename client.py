import sys
import os
import time
import redis.asyncio as redis
import pickle
import signal
import asyncio
import logging
import websockets.client
import traceback

from threading import Event
from typing import Annotated
from queue import (
    SimpleQueue,
    Empty,
)

from src.redis_monitor import RedisMonitor
from src.worker_mirroring_writer import WorkerMirroringWriter
from src.worker_mirroring_reader import WorkerMirroringReader
from src.worker_bulk_sync_writer import WorkerBulkSyncWriter
from src.worker_bulk_sync_reader import WorkerBulkSyncReader
from src.worker_websocket_event_reader import WorkerWebsocketEventReader
from src.command_parser import CommandParser
from src.header_parser import HeaderParser
from src.async_simple_queue import AsyncSimpleQueue

from fastapi import (
    Cookie,
    Depends,
    FastAPI,
    Query,
    WebSocket,
    WebSocketException,
    Response,
    status,
)
from fastapi.responses import HTMLResponse

from starlette.websockets import WebSocketState, WebSocketDisconnect
from websockets.exceptions import ConnectionClosedOK

from asyncer import asyncify, syncify

app = FastAPI()

redis_url = 'redis://%s:%s/?protocol=3&socket_connect_timeout=5&socket_timeout=10&socket_keepalive=true&retry_on_timeout=false' % (os.getenv('REDIS_TARGET_HOST', 'redis'), os.getenv('REDIS_TARGET_PORT', '6379'))
websocket_base_url = os.getenv('WEBSOCKET_URL', 'ws://172.17.0.1:8088/ws')
websocket_token = os.getenv('WEBSOCKET_TOKEN', 'token')
websocket_url = '%s?token=%s' % (websocket_base_url, websocket_token)
websocket_sync_command = os.getenv('WEBSOCKET_SYNC_COMMAND', 'sync')
websocket_mirror_command = os.getenv('WEBSOCKET_MIRROR_COMMAND', 'mirror')
websocket_event_command = os.getenv('WEBSOCKET_EVENT_COMMAND', 'event')

context = {}

context['target_connection'] = None


def wait_for_worker_bulk_sync_writer(worker_bulk_sync_writer: WorkerBulkSyncWriter):
    worker_bulk_sync_writer.event_finished.wait()


class EventBackgroundRunner:
    def __init__(self):
        self.event_should_stop: Event = Event()
        self.event_finished: Event = Event()
        self.websocket = None

        self.mirroring_started = False
        self.last_received = None
        self.last_seen = 0

        self.redis_client = context['target_connection']
        self.websocket_url = websocket_url

        self.worker_mirroring_writer = None

    async def on_init_redis(self):
        self.redis_client = context['target_connection']
        self.last_received = None
        try:
            self.last_received = await self.redis_client.get('__redis-sync-last-received')
            self.last_received = int(self.last_received.decode())
        except:
            self.last_received = None

        self.last_seen = 0
        try:
            self.last_seen = await self.redis_client.get('__redis-sync-last-seen')
            self.last_seen = float(self.last_seen.decode())
        except:
            self.last_seen = 0

    async def on_init_websocket(self):
        try:
            self.websocket = await websockets.client.connect(self.websocket_url, open_timeout=10, ping_interval=120, ping_timeout=120, close_timeout=120, max_size=134217728, max_queue=256, read_limit=134217728, write_limit=134217728)

            await self.websocket.send(websocket_event_command)
            data = await self.websocket.recv()
            if data != 'OK':
                await self.websocket.close()
                raise
            print("last_received: '%s'" % ('' if self.last_received is None else str(self.last_received)))

            await self.websocket.send('' if self.last_received is None else str(self.last_received))
            data = await self.websocket.recv()
            if data != 'OK':
                await self.websocket.close()
                raise

        except:
            try:
                await self.websocket.close()
            except:
                pass
            self.websocket = None
            raise

    async def on_init_websocket_reader(self):
        self.input_finished = False
        self.worker_websocket_event_reader = WorkerWebsocketEventReader(input_websocket=self.websocket, loop=asyncio.get_event_loop())
        self.worker_websocket_event_reader.daemon = True
        #worker_websocket_event_reader.debug = True
        self.worker_bulk_sync_writer_queue = self.worker_websocket_event_reader.subscribe(['syncdata'], only_payload=True)
        self.worker_mirroring_writer_queue = self.worker_websocket_event_reader.subscribe(['mirrordata'], only_payload=True)
        self.worker_events_queue = self.worker_websocket_event_reader.subscribe(['starting', 'finished'])
        self.worker_websocket_event_reader.start()

        

    async def on_init_bulk_sync_writer(self):
        
        self.worker_bulk_sync_writer = WorkerBulkSyncWriter(output_redis_client=self.redis_client, input_queue=self.worker_bulk_sync_writer_queue, loop=asyncio.get_event_loop())
        self.worker_bulk_sync_writer.daemon = True
        self.worker_bulk_sync_writer.subscribe(['completed', 'finished', 'disconnect'], queue=self.worker_events_queue)
        self.worker_bulk_sync_writer.start()

    async def on_starting_websocket_reader(self):
        return await self.on_init_bulk_sync_writer()

    async def on_finished_websocket_reader(self):
        self.worker_websocket_event_reader = None
        if self.worker_bulk_sync_writer is not None:
            await self.worker_bulk_sync_writer.close()
        if self.worker_mirroring_writer is not None:
            await self.worker_mirroring_writer.close()

    async def on_finished_bulk_sync_writer(self):
        self.worker_bulk_sync_writer = None
        if not self.input_finished and self.worker_websocket_event_reader is not None:
            await self.worker_websocket_event_reader.close()

    async def on_finished_mirroring_writer(self):
        self.worker_mirroring_writer = None
        if self.worker_websocket_event_reader is not None:
            await self.worker_websocket_event_reader.close()

    async def on_completed_bulk_sync_writer(self):
        self.input_finished = True
        await self.on_init_mirror_writer()

    async def on_finished_client(self):
        if self.worker_websocket_event_reader is not None:
            await self.worker_websocket_event_reader.close()

    async def on_init_mirror_writer(self):
        self.worker_mirroring_writer = WorkerMirroringWriter(output_redis_client=self.redis_client, input_queue=self.worker_mirroring_writer_queue, loop=asyncio.get_event_loop())
        self.worker_mirroring_writer.daemon = True
        self.worker_mirroring_writer.subscribe(['starting', 'finished', 'connect', 'disconnect'], queue=self.worker_events_queue)
        self.worker_mirroring_writer.start()


    async def run(self):

        await self.on_init_redis()
        await self.on_init_websocket()
        await self.on_init_websocket_reader()

        while True:
            event, payload = await self.worker_events_queue.get()
            print(event)

            if payload is self.worker_websocket_event_reader:
                if event == 'starting':
                    await self.on_starting_websocket_reader()
                elif event == 'finished':
                    await self.on_finished_websocket_reader()
                    if self.worker_bulk_sync_writer is None and self.worker_websocket_event_reader is None and self.worker_mirroring_writer is None:
                        break
            elif payload is self.worker_bulk_sync_writer:
                if event == 'finished':
                    await self.on_finished_bulk_sync_writer()
                    if self.worker_bulk_sync_writer is None and self.worker_websocket_event_reader is None and self.worker_mirroring_writer is None:
                        break
                elif event == 'completed':
                    await self.on_completed_bulk_sync_writer()
                elif event == 'disconnect':
                    if self.worker_bulk_sync_writer is not None:
                        await self.worker_bulk_sync_writer.close()
            elif payload is self.worker_mirroring_writer:
                if event == 'starting':
                    continue
                elif event == 'finished':
                    await self.on_finished_mirroring_writer()
                    if self.worker_bulk_sync_writer is None and self.worker_websocket_event_reader is None and self.worker_mirroring_writer is None:
                        break
                elif event == 'connect':
                    continue
                elif event == 'disconnect':
                    await self.on_init_redis()
                    if self.worker_websocket_event_reader is not None:
                        await self.worker_websocket_event_reader.close()
            else:
                print(event)

        await self.on_finished_client()
        self.event_finished.set()
        os.kill(os.getpid(), signal.SIGTERM)

    async def close(self):
        try:
            self.event_should_stop.set()
            await self.websocket.close()
        except:
            pass

        try:
            if self.worker_websocket_event_reader is not None:
                await self.worker_websocket_event_reader.close()
        except:
            pass

        try:
            if self.worker_bulk_sync_writer is not None:
                await self.worker_bulk_sync_writer.close()
        except:
            pass

        try:
            if self.worker_mirroring_writer is not None:
                await self.worker_mirroring_writer.close()
        except:
            pass

runner = EventBackgroundRunner()


@app.on_event("startup")
async def startup_event():
    context['target_connection'] = await redis.from_url(redis_url)
    asyncio.create_task(runner.run())

@app.on_event("shutdown")
async def shutdown_event():
    await runner.close()
    await context['target_connection'].close()
    try:
        await asyncify(runner.event_finished.wait)(timeout=10)
    except:
        pass


def get_cookie_or_token(
    websocket: WebSocket,
    session: Annotated[str | None, Cookie()] = None,
    token: Annotated[str | None, Query()] = None,
):
    if session is None and token is None:
        raise WebSocketException(code=status.WS_1008_POLICY_VIOLATION)
    return token or session



async def check_websocket(websocket: WebSocket):
    try:
        # Check if connection is active
        await asyncio.wait_for(websocket.receive_bytes(), timeout=0.1)
        return True
    except asyncio.TimeoutError:
        return True
    except WebSocketDisconnect:
        # Connection closed by client
        print("disconnect")
    else:
        return False


@app.head("/status")
@app.get("/status")
def get_status(response: Response):
    mirroring = runner.worker_mirroring_writer is not None
    websocket = runner.websocket is not None
    response.status_code = 200 if mirroring and websocket else 503
    return {"mirroring": mirroring, "websocket": websocket}

@app.head("/health")
@app.get("/health")
def get_health():
    return {"health": True}

FROM python:3.12.1-slim-bookworm AS base

RUN apt-get update && apt-get install -y wget

# Default user 1000
USER 1000

# Home dir
WORKDIR /code
ENV HOME=/code

# Install requirements
COPY --chown=1000:1000 ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt



# Runtime image
FROM base AS runtime

ENV HOME=/code
ENV PATH="$PATH:/code/.local/bin"
ENV PYTHONUNBUFFERED=1

# Python script in /code/app
WORKDIR /code/app

COPY --chown=1000:1000 ./assets /code/app/assets
COPY --chown=1000:1000 ./src /code/app/src
COPY --chown=1000:1000 ./*.py /code/app/



FROM runtime AS runtime-server

EXPOSE 8088

# Run with uvicorn
CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "8088", "--ws-max-size", "134217728", "--ws-max-queue", "128", "--workers", "1"]
#CMD ["python", "main.py"]



FROM runtime AS runtime-client

EXPOSE 8089

# Run with uvicorn
CMD ["uvicorn", "client:app", "--host", "0.0.0.0", "--port", "8089","--ws-max-size", "134217728", "--ws-max-queue", "128", "--workers", "1"]
